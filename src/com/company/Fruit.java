package com.company;

/**
 * Created by Brayan on 25-Jun-17.
 */
public class Fruit {
    private String color;
    private String forma;
    private String estado;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getForma() {
        return forma;
    }

    public void setForma(String forma) {
        this.forma = forma;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
