package com.company;

import com.sun.org.apache.xpath.internal.operations.Or;

public class Main {

    public static void main(String[] args) {

        Object objeto = new Object();
        Fruit fruta = new Fruit();
        Fruit fruta1= new Fruit();
        Apple manzana = new Apple();
        Citrus citrico = new Citrus();
        Orange naranja = new Orange();

        try {
            //OBJECT

            fruta = (Fruit)objeto;
            manzana=(Apple)objeto;
            naranja= (Squeezable)objeto;
            citrico=(Citrus)objeto;
            naranja=(Orange)objeto;

            //FRUIT

            fruta = (Fruit)fruta1;
            manzana=(manzana)fruta1;
            naranja=(Squeezable)fruta1;
            naranja=(Orange)fruta
            citrico = (Citrus) fruta;
            //APPLE
            fruta = (Fruit)manzana;
            manzana=(Apple)manzana;
            naranja=(Squeezable)manzana;
            citrico = (Citrus) manzana;
            naranja=(Orange)manzana;

            //CITRUS
            fruta = (Fruit)citrico;
            manzana=(Apple)citrico;
            naranja = (Squeezable) citrico;
            citrico=(Citrus)citrico;
            naranja=(Orange)citrico;
            //ORANGE
            fruta=(Fruit)naranja;
            manzana=(Apple)naranja;
            naranja=(Squeezable)naranja;
            citrico = (Citrus) naranja;
            manzana = (Apple) naranja;
            
            System.out.println("Cheking cast FruitObject: OK");


            System.out.println("Cheking cast FruitApple: OK");

            System.out.println("Cheking cast FruitApple: OK");

            System.out.println("Cheking cast ObjectFruit: OK");

            System.out.println("Cheking cast OrangeCitrus: OK");

            System.out.println("Cheking cast CitrusOrange: OK");
               // no se puede realizar el cast de manzana a Citrus
            System.out.println("Cheking cast CitrusApple: OK");

            System.out.println("Cheking cast CitrusFruit: OK");
              // no se puede realizar el cast de naranja a Apple
            System.out.println("Cheking cast AppleOrange: OK");

            System.out.println("Cheking cast FruitCitrus: OK");

        }
        catch (ClassCastException ex){
            System.out.println(ex.getMessage());
        }



    }
}
